Autoreq: 0

Name:           puppet-augeasproviders_core
Version:        1.0
Release:        5%{?dist}
Summary:        Puppetlabs augeasproviders_core module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs augeasproviders_core module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_core/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_core/
touch %{buildroot}/%{_datadir}/puppet/modules/augeasproviders_core/supporting_module

%files -n puppet-augeasproviders_core
%{_datadir}/puppet/modules/augeasproviders_core
%doc code/README.md

%changelog
* Mon Jan 15 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.0-5
- Rebased to #4e0d50e767d7dcee1936c96e7e50ec8649645b39 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-4
- Bump release for disttag change

* Thu Apr 15 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-3
- Use Autoreq to ignore incorrect dependency on rspec

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- fix requires for puppet-agent

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 1.0-1
-Initial release
